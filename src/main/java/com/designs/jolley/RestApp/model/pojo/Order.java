package com.designs.jolley.RestApp.model.pojo;

import lombok.Data;

import javax.persistence.*;

/**
 * Created by Luke Dutton on 10/26/18
 *
 * @author Luke Dutton
 * @since 10/26/18
 */
@Entity
@Data
@Table(name = "CUSTOMER_ORDER")
public class Order {

    private @Id @GeneratedValue Long id;

    private String description;
    private Status status;
    private Long employeeId;

    public Order(String description, Status status, Long employeeId){
        this.description = description;
        this.status = status;
        this.employeeId = employeeId;
    }
}
